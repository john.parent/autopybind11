# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())  # noqa

import simpleTest as simple
# Import of test_base_double from additional causes conflict on run
# from additional.additional import nonTemplate

class enumerationTests(unittest.TestCase):

    def test_Plain(self):
        # Test plain enumeration
        # names are exactly as found in C++
        # Values are only accessible via the enumeration object
        plain = simple.enumPlain
        self.assertEqual(plain.val1, 1)
        self.assertEqual(plain.val2, 2)
        self.assertEqual(plain.val3, 3)

    # Test customized values
    # Some names are changed based on YAML file
    # Values are only accessible via the enum object
    def test_CustomVals(self):
        cust = simple.enumCustVal
        self.assertEqual(cust.uno, 1)
        self.assertEqual(cust.deux, 2)
        self.assertEqual(cust.three, 3)

    # Test exported values
    # names are exactly as found in C++
    # Values can be accessed both at the module level
    # and from the enumeration object
    def test_ExportedVals(self):
        exported = simple.enumWithExport
        self.assertIn("exportVal0", dir(simple))
        self.assertIn("exportVal1", dir(simple))
        self.assertEqual(exported.exportVal0, simple.exportVal0)
        self.assertEqual(exported.exportVal1, simple.exportVal1)

    # Test both customizations at once
    def test_ExportedVals(self):
        both = simple.enumWithBoth
        self.assertIn("both0", dir(simple))
        self.assertIn("both1", dir(simple))
        self.assertEqual(both.both0, simple.both0)
        self.assertEqual(both.both1, simple.both1)
        self.assertEqual(both.kBothTogether, 3)
        self.assertEqual(simple.kBothTogether, 3)

if __name__ == '__main__':
    testRunner = unittest.main(__name__, argv=['main'], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
