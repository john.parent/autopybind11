#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include "${input_dir}/sample.hpp"




namespace py = pybind11;
void apb11_smoke_test_A_py_register(py::module &m)
{

  py::class_<::A> PyA(m, "A");

    PyA.def(py::init<>())
    .def(py::init<::A const &>(),py::arg("arg0"))
    
    
    
    ;
}
