# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
cmake_minimum_required(VERSION 3.15)
project(multi_namespace CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_library(multi_namespaced INTERFACE)
target_sources(multi_namespaced INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/no_namespace.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/first_namespace.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/second_namespace.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/compound_namespace.hpp)
target_include_directories(multi_namespaced INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_add_module("multi_namespaced_module"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES multi_namespaced
                       )

