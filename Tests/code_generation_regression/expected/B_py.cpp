#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include "${input_dir}/sample.hpp"




namespace py = pybind11;
void apb11_smoke_test_B_py_register(py::module &m)
{

  py::class_<::B> PyB(m, "B");

    
    PyB.def(py::init<>())
    .def(py::init<::B const &>(),py::arg("arg0"))
    .def("Alpha", static_cast<::A ( ::B::* )(  )const>(&::B::Alpha))
    .def("Beta", static_cast<::A ( ::B::* )(  )const>(&::B::Beta))
    .def("Something", static_cast<::A ( ::B::* )(  )const>(&::B::Something))
    
    
    ;
}
