cmake_minimum_required(VERSION 3.15)
project(xml_input CXX)

add_library(xmlInput xml_input.cxx)
add_custom_target(
  generate_xml ALL
  COMMAND castxml --castxml-output=1 ${CMAKE_CURRENT_SOURCE_DIR}/xml_input.cxx
)

add_dependencies(xmlInput generate_xml)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})
set_property(TARGET xmlInput PROPERTY POSITION_INDEPENDENT_CODE ON)
autopybind11_add_module("xmlInputTest" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/xml_input.yml
                        XML_INPUT ${CMAKE_CURRENT_BINARY_DIR}/xml_input.xml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES xmlInput)
