#ifndef BASE_H
#define BASE_H

#include <string>

class Base
{
public:
    virtual inline int get_var() = 0;
    virtual inline std::string hello() = 0;
protected:
    int var_base;
};


#endif // BASE_H