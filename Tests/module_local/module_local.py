# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())

from math_module import math_mod
from sci_module import sci_mod


class moduleLocalTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_sci_mod(self):
        vector_add = sci_mod.sci.vector_addition()
        vec1 = [0, 1, 2, 3, 4]
        vec2 = [4, 3, 2, 1, 0]
        self.assertListEqual(vector_add.add_vecs(vec1, vec2), [4, 4, 4, 4, 4])

    def test_math_mod_construct(self):
        math_mod.mather.multy()
        math_mod.mather.multy(1, 2)

    def test_math_mod_mult(self):
        mult = math_mod.mather.multy(5, 5)
        self.assertEqual(mult.multiplier(), 25)

    def test_math_mod_setter(self):
        mult = math_mod.mather.multy()
        self.assertEqual(mult.multiplier(), 0)
        mult.setter(4, 5)
        self.assertEqual(mult.multiplier(), 20)


if __name__ == "__main__":
    testRunner = unittest.main(__name__, argv=["main"], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
