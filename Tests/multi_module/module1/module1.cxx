/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#include "module1.hpp"


nonTemplate::nonTemplate() {};

double nonTemplate::div(double v1, double v2)
{
  return v1 / v2;
}


float nonTemplate::fdiv(float v1, float v2)
{
  return v1 / v2;
}

