# Class that helps with customization of C++ functions, enums, classes, etc.
class Customizer:
    def __init__(self, mod_tree):
        self.module_structure = mod_tree

    def fetch_customization_field(self, yaml_dict, field):
        if (
            yaml_dict
            and "customization" in yaml_dict
            and yaml_dict["customization"]
        ):
            cust_dict = yaml_dict["customization"]
            if field in cust_dict and cust_dict[field]:
                return cust_dict[field]
        return None

    # Helper method to import a mapping of templated memeber functions to
    # their specific instantiation types from the wrapper_yaml
    def get_method_inst(self, yaml_dict):
        return self.fetch_customization_field(yaml_dict, "method_inst")

    # Accepts a yaml dictionary for a class which contains a dictionary of
    # attribute names which point to a string of pybind11 code
    # Returns None if not found
    def get_attribute_dict(self, yaml_dict):
        name_field = self.fetch_customization_field(yaml_dict, "attributes")
        return name_field if name_field else None

    # Accepts a yaml dictionary for a class, enum, etc. and
    # extracts the custom name from the customization field, if both exist.
    # Returns None if not found
    def get_custom_name(self, yaml_dict):
        name_field = self.fetch_customization_field(yaml_dict, "name")
        return name_field if name_field else None

    def get_custom_enum_vals(self, yaml_dict):
        enum_field = self.fetch_customization_field(
            yaml_dict, "custom_enum_vals"
        )
        return enum_field if enum_field else {}

    # TODO Support arbitrary levels of nesting within these two and all enums in general
    def get_custom_internal_enum_name(self, yaml_dict, name):
        internal_enum_field = self.fetch_customization_field(
            yaml_dict, "enums"
        )
        cust_name = None
        if internal_enum_field and "name" in internal_enum_field[name]:
            cust_name = internal_enum_field[name]["name"]
        return cust_name

    def get_custom_internal_enum_vals(self, yaml_dict, name):
        internal_enum_field = self.fetch_customization_field(
            yaml_dict, "enums"
        )
        if (
            internal_enum_field
            and "custom_enum_vals" in internal_enum_field[name]
        ):
            return internal_enum_field[name]["custom_enum_vals"]
        return {}

    def get_custom_enum_export(self, yaml_dict):
        if (
            yaml_dict
            and "customization" in yaml_dict
            and yaml_dict["customization"]
        ):
            if "export_enum_vals" in yaml_dict["customization"]:
                return True
        return False

    def get_module_local_value(self, yaml_dict):
        if (
            yaml_dict
            and "customization" in yaml_dict
            and yaml_dict["customization"]
        ):
            if "module_local" in yaml_dict["customization"]:
                return True
        return False

    def get_custom_holder_type(self, yaml_dict):
        return self.fetch_customization_field(yaml_dict, "custom_holder_type")

    def get_pass_by_ref(self, yaml_dict):
        pass_by = self.fetch_customization_field(yaml_dict, "pass_by_ref")
        if pass_by:
            return True
        return False

    def get_skiplisted_members(self, yaml_dict):
        skiplist_dict = self.fetch_customization_field(yaml_dict, "skiplist")
        if skiplist_dict:
            skiplist = {}
            for option in "member_functions", "member_variables", "operators":
                if option in skiplist_dict and skiplist_dict[option]:
                    for member in skiplist_dict[option]:
                        if type(member) is dict:
                            skiplist = {**skiplist, **member}
                        else:
                            skiplist[member] = ""
            return skiplist
        return {}

    def get_curr_mod_name(self, yaml_dict):
        cust_namespace_structure = self.fetch_customization_field(
            yaml_dict, "namespace"
        )
        if cust_namespace_structure:
            return cust_namespace_structure.split(".")[-1]
        return None

    def get_keepalive(self, yaml_dict):
        keep_alive_list = self.fetch_customization_field(
            yaml_dict, "keep_alive"
        )
        return keep_alive_list if keep_alive_list else []

    # If c++ object is given a custom namespace location
    # extract absolute namespace path, return namespace
    # and location in yaml representation of hierarchy
    def add_custom_namespace(self, custom_namespace):
        namespace_structure = custom_namespace.split(".")
        curr_nmspc = namespace_structure[-1]
        mod_structure = self.module_structure
        if not self.find_and_insert_namespace(
            mod_structure, iter(namespace_structure), curr_nmspc
        ):
            self.module_structure[curr_nmspc] = {}

    def get_add_custom_namespace(self, yaml_dict):
        cust_namespace = self.fetch_customization_field(yaml_dict, "namespace")
        if cust_namespace is not None:
            self.add_custom_namespace(cust_namespace)
        return cust_namespace.split(".")[-1] if cust_namespace else None

    # name_strucuture is a dict object representing current location
    # in custom namespace structure (initialized as module level)
    # address is an iterator over a list of namespace addresses
    # representing the absolute namespace path
    def find_and_insert_namespace(
        self, name_structure, address, lowest_address
    ):
        try:
            try:
                nxt_adr = next(address)

            except StopIteration:
                # we have reached the end of this namespace declaration
                # without reaching an unknown namespace, return True
                # if address is not in the current namespace structure, return false
                # so we can add it
                return lowest_address in name_structure
            is_final = nxt_adr == lowest_address
            name_structure = name_structure[nxt_adr]
            if is_final:
                ret = True
            else:
                ret = self.find_and_insert_namespace(
                    name_structure, address, lowest_address
                )
        except KeyError:
            if not is_final:
                # we've had a failure here, one part of the absolute namespace is wrong
                # report that fact
                ret = False
            else:
                # we've found the location of this new namespace within the larger structure,
                # add it, and report
                name_structure[nxt_adr] = {}
                ret = True
        return ret

    def get_structure(self):
        if not self.module_structure:
            return None
        return self.module_structure
