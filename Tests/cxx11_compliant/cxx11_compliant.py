import os, sys, unittest

sys.path.append(os.getcwd())

from compliance_mod import (
    Base,
    UniqueHolder,
    sharedHolder,
    Base2,
    take_your_ptr,
)


class TestBase(unittest.TestCase):
    def test_constructor(self):
        Base()
        Base(4, 8)
        Base(4)

    def test_methods(self):
        b = Base(4, 8)
        self.assertEqual(b.size(), 4)
        self.assertEqual(b.sum(), 8)
        arr = b.get_arr()
        self.assertEqual(8, arr)


class TestsharedHolder(unittest.TestCase):
    def test_constructor(self):
        sharedHolder()
        sharedHolder(Base())

    def test_methods(self):
        b = Base(4, 8)
        s = sharedHolder(b)
        self.assertEqual(s.sum(), 8)
        self.assertEqual(s.size(), 4)

    def test_shared_base(self):
        b = Base(4, 8)
        s = sharedHolder(b)
        b1 = s.base_arr()
        self.assertIs(b1, b)
        a = s.ret_int(1)
        self.assertEqual(a, 1)

    def test_shared_stl(self):
        b = sharedHolder(Base(4, 8))
        v = b.get_vec()
        new_v = [1.0, 2.0, 3.0]
        b.set_vec(new_v)
        new_v[1] = 4.0
        new_b = b.get_vec()
        old_v = b.ret_vec(new_v)


class TestUniqueHolder(unittest.TestCase):
    def test_constructor(self):
        UniqueHolder()
        UniqueHolder(3)

    def test_methods(self):
        u = UniqueHolder(999)
        self.assertIsInstance(u.give(), Base2)
        u.take(3)
        a = 3
        b = u.give_back_int(a)
        self.assertEqual(a, b)
        c = Base2()
        d = u.give_back_u_base(c)
        self.assertIsInstance(d, Base2)


class TestFreeFunction(unittest.TestCase):
    def test_free_function(self):
        take_your_ptr(1)


if __name__ == "__main__":
    unittest.main()
