/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt      */
#include <string>
#include <iostream>

class printer
{
public:
    printer() {};
    printer(std::string message) : m_(message) {}; 
    void print() {std::cout << m_ << std::endl;};
private:
    std::string m_;
};