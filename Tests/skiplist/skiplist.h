/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef SKIPLIST_H
#define SKIPLIST_H

#include <tuple>
#include <string>

namespace simple{
    class SimpleClass
    {
    public:
        SimpleClass() {};
        SimpleClass(int a, std::string str) : internal_int(a), 
                                              internal_str(str) {};

        std::tuple<int, std::string> get_vals();

        int get_int_val();
        std::string get_str_val();

        void set_int_val(int a);
        void set_str_val(std::string str);
        
        void print_internals();
        
        void wrong_one(int t);
        virtual void found();
        virtual void notFound();


        int operator()();
        SimpleClass operator-();
        SimpleClass operator-(SimpleClass& other);

        const std::string external_str = "foobar";
        const int external_int = 42;
        const int public_member = 12;
    protected:
        void im_protected_and_excluded();
    private:
        int internal_int;
        std::string internal_str;

    };
    class SecondClass
    {
    public:
        SecondClass() {};
        SecondClass(int a, std::string str) : internal_int(a),
                                              internal_str(str) {};

        const int external_int = 42;
    private:
        int internal_int;
        std::string internal_str;

    };
}


#endif // SKIPLIST_H