import os, sys, unittest

import ref_test_mod
from ref_test_mod import SimpleMath, SimpleStringOps


class TestSimpleMathMod(unittest.TestCase):
    def test_methods_ref(self):
        a = 2
        b = 2
        c = 4
        d = 0
        e = SimpleMath.c_pow(a, b, d)
        self.assertEqual(e[2], 4)
        a = SimpleMath.incr(a)
        self.assertEqual(a[0], 3)
        b = SimpleMath.decr(b)
        self.assertEqual(b[0], 1)
        a, q, r = SimpleMath.div_r(7, 2)
        self.assertEqual(a, 7)
        self.assertEqual(q, 3)
        self.assertEqual(r, 1)


class TestSimpleStringOps(unittest.TestCase):
    def test_string_methods_ref(self):
        lower_str = "hello world"
        upper_str = "HELLO WORLD"
        str1 = "cat"
        str2 = "dog"
        str1, str2 = SimpleStringOps.swap(str1, str2)
        self.assertEqual(str1, "dog")
        self.assertEqual(str2, "cat")
        cap_string = SimpleStringOps.capitalize(lower_str)
        self.assertEqual(cap_string[0], upper_str)


class TestFreeFunction(unittest.TestCase):
    def test_ff(self):
        a = 1
        b = 2
        sum_ = 0
        sum_ = ref_test_mod.add(a, b, sum_)
        self.assertEqual(sum_[2], 3)


if __name__ == "__main__":
    unittest.main()
