#ifndef eigen_matrix_hpp
#define eigen_matrix_hpp

// This example borrow from PyBind11 at https://pybind11.readthedocs.io/en/stable/advanced/cast/eigen.html

#include <Eigen/Core>

class Eigen_Class {
    Eigen::MatrixXd big_mat = Eigen::MatrixXd::Zero(10000, 10000);
public:
    Eigen::MatrixXd &getMatrix() { return big_mat; }
    const Eigen::MatrixXd &viewMatrix() { return big_mat; }
    void scale_by_2(Eigen::VectorXd v) { v *= 2;}
    Eigen::MatrixXd getGivenMat(Eigen::MatrixXd const mat) {return mat;}

};


inline Eigen::MatrixXd transpose(Eigen::MatrixXd mat) {return mat.transpose();}
inline void transpose_in_place(Eigen::MatrixXd mat) {mat.transposeInPlace();}
#endif //eigen_matrix_hpp