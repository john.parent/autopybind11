#include "container.h"

Int Container::at(size_t pos)
{
    return this->arr.at(pos);
}

const Int Container::at(size_t pos) const
{
    return this->arr.at(pos);
}

size_t Container::size() const
{
    return this->len;
}

bool Container::empty() const
{
    return this->arr.empty();
}

Int Container::ret(Int type)
{
    return type;
}

const Int Container::ret(Simple_Int type) const
{
    return type;
}
