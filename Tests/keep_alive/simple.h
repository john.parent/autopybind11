#ifndef simple_h
#define simple_h
class simple
{
public:
  simple();
  simple(int var1, int optional=10);
  simple(int var1, char mandatory, double opt2);
  int hello();
};

void outside();
#endif
