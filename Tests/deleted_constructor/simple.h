#ifndef simple_h
#define simple_h

#include <functional>
#include <memory>
#include <ostream>
#include <set>
#include <string>
#include <utility>

// Formula Cell class lovingly stolen from
//  https://github.com/RobotLocomotion/drake/blob/master/common/symbolic_formula_cell.h
//
class FormulaCell {
 public:
  virtual bool EqualTo(const FormulaCell& c) const = 0;
  /** Checks ordering. */
  virtual bool Less(const FormulaCell& c) const = 0;
  virtual std::ostream& Display(std::ostream& os) const = 0;

 protected:
  /** Default constructor (deleted). */
  FormulaCell() = delete;
  /** Move-construct a formula from an rvalue. */
  FormulaCell(FormulaCell&& f) = default;
  /** Copy-construct a formula from an lvalue. */
  FormulaCell(const FormulaCell& f) = default;
  /** Copy-construct a formula from an lvalue. */
  explicit FormulaCell(int k);
  /** Move-assign (DELETED). */
  FormulaCell& operator=(FormulaCell&& f) = delete;
  /** Copy-assign (DELETED). */
  FormulaCell& operator=(const FormulaCell& f) = delete;
  /** Default destructor. */
  virtual ~FormulaCell() = default;

};
#endif
