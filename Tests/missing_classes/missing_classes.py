# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import deletedTest

# Import of test_base_double from additional causes conflict on run
# from additional.additional import nonTemplate


class deletedTests(unittest.TestCase):
    def test_missing_module(self):
        self.assertIn("missing_base", dir(deletedTest))
        self.assertEqual(
            str(deletedTest.missing_base.__bases__),
            "(<class 'pybind11_builtins.pybind11_object'>,)",
        )
        self.assertNotIn("not_wrapped", dir(deletedTest))
        self.assertIn("found_class", dir(deletedTest))


if __name__ == "__main__":
    testRunner = unittest.main(__name__, argv=["main"], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
