#include "unique.h"

UniqueHolder::UniqueHolder()
{
    this->b = std::unique_ptr<int>(new int());
}

UniqueHolder::UniqueHolder(std::unique_ptr<int> b)
{
    this->b = std::move(b);
}

void UniqueHolder::take(std::unique_ptr<int> new_b)
{
    this->b = std::move(new_b);
}

std::unique_ptr<Base2> UniqueHolder::give()
{
    return std::unique_ptr<Base2>{new Base2()};
}


void take_your_ptr(std::unique_ptr<int> int_ptr)
{
    auto mine = std::move(int_ptr);
}