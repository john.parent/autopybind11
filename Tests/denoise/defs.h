#ifndef DEF
#define DEF

#include <Eigen/Core>

namespace First{

enum CouplingTag {
    High,
    Mid,
    Low
};

template <CouplingTag T = CouplingTag::Low>
class Coupler
{
public:
    Coupler() {};
private:
    CouplingTag couple_level;
};

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RowMatrixXd;
typedef Coupler<CouplingTag::Mid> MidCouple;

}

#endif