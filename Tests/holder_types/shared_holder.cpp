#include "shared_holder.h"

sharedHolder::sharedHolder()
{
    this->b = std::shared_ptr<Base>(new Base());
    this->v = std::shared_ptr<std::vector<double>>(new std::vector<double>());
}

sharedHolder::sharedHolder(std::shared_ptr<Base> b)
{
    this->b = b->shared_from_this();
    this->v = std::shared_ptr<std::vector<double>>(new std::vector<double>());
}

const int sharedHolder::sum()
{
    return this->b->sum();
}

const int sharedHolder::size()
{
    return (int)this->b->size();
}

std::shared_ptr<Base> sharedHolder::base_arr()
{
    return this->b;
}

std::shared_ptr<std::vector<double>> sharedHolder::get_vec()
{
    return this->v;
}

void sharedHolder::set_vec(std::shared_ptr<std::vector<double>> new_v)
{
    this->v = new_v;
}
