/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef NO_NAMESPACE_HPP
#define NO_NAMESPACE_HPP

class no_nmspc_class
{
public:

  inline no_nmspc_class()
  {
    var1 = 0;
    var2 = 0;
  }

  inline void decrement_vars( int update1, double update2 )
  {
    var1 -= update1;
    var2 -= update2;
  }

  inline void increment_vars( int update, double update2 )
  {
    var1 += update;
    var2 += update2;
  }
  inline int sum_vars()
  {
    return var1 + (int)var2;
  }
private:
  int var1;
  double var2;
};

inline int no_nmspc_func( int val1, int val2 )
{
  return val1 + val2;
}

#endif