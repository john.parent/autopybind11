#ifndef simple_h
#define simple_h
class simple
{
public:
  simple();
  simple(int var1, int optional=10);
  int hello();
  enum Values { Val1, Val2, Val3};
};

#endif
