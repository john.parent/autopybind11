cmake_minimum_required(VERSION 3.15)
project(eigen CXX)
enable_testing()

add_executable(eigen-test eigen_test.cpp)
target_include_directories(eigen-test PRIVATE ${Eigen_INCLUDE_DIR} )

add_test(NAME Eigen-Dependency-Test COMMAND eigen-test)
