cmake_minimum_required(VERSION 3.12)

set(FILE_NAME "eigen-src")
set(EIGEN_URL "https://gitlab.com/libeigen/eigen/-/archive/3.3.9/")
set(EIGEN_TARBALL "eigen-3.3.9.tar")
set(EIGEN_MD5 "2543791ab461dd263816e9f8e8fec508")

file(DOWNLOAD
    "${EIGEN_URL}${EIGEN_TARBALL}"
    ".gitlab/${EIGEN_TARBALL}"
    STATUS eigen_download_status
    EXPECTED_MD5 ${EIGEN_MD5}
    )

list(GET eigen_download_status 0 res)
if (res)
    list(GET download_status 1 err)
    message(FATAL_ERROR
    "Failed to download ${EIGEN_TARBALL} from ${EIGEN_URL}: ${err}")
endif ()


execute_process(
    COMMAND
        "${CMAKE_COMMAND}"
        -E tar
        xzf "./.gitlab/${EIGEN_TARBALL}"
    RESULT_VARIABLE res
    ERROR_VARIABLE err
    ERROR_STRIP_TRAILING_WHITESPACE
    )

if (res)
    message(FATAL_ERROR
        "Failed to extract ${FILE_NAME}: ${err}")
endif ()
