#ifndef SIMPLE_MATH
#define SIMPLE_MATH

#include <math.h>

class SimpleMath
{
public:
    static inline void c_pow(const int &a, const int &b, int &ret) { ret = (int)pow((double)a,(double)b); }
    static inline void incr(int &a) { a++; }
    static inline void decr(int &a) { a--; }
    static inline int div_r(const int &a, int &b) { b = a/b; return a%b;}
};

static inline void add(const int &a,const int &b, int &ret) {ret = a+b;}

#endif // SIMPLE_MATH