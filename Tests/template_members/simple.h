#ifndef simple_h
#define simple_h


class simple
{
public:
  simple();
  template <typename T>
  explicit simple(T data) { }
  int hello();
  template<typename N>
  N hello(N input) {return input;};
};

class basic
{
public:
  basic() {}
};

using new_alias = simple;

template <typename T>
class aliasClass
{
public:
  aliasClass();
  T getTemplate() { return T();};
  int simple() {return 12;};
};

using templ_alias = aliasClass<basic>;

template <typename T, typename R>
class aliasDoubleClass
{
public:
  aliasDoubleClass();
  T getTemplate() { return T();};
  int simple() {return 12;};
};

template <typename T, typename Q>
class simple2
{
public:
  simple2() {};
};

template <typename F>
class simpleTemplate
{
public:
  simpleTemplate() {};
  template <typename N>
  explicit simpleTemplate(N data) {};
  int non_templ_member() {return 10;};
  template <typename N>
  N simple(N input) {return input;};
  int simple() {return 12;};
};

#endif
