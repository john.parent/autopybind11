#ifndef UNIQUE_H
#define UNIQUE_H

#include <memory>
#include "shared_base.hpp"

class Base2
{
public:
    Base2() {}
};

typedef std::unique_ptr<int> u_int_ptr;
using ub2 = std::unique_ptr<Base2>;
class UniqueHolder
{
public:
    UniqueHolder();
    UniqueHolder(std::unique_ptr<int> b);
    std::unique_ptr<Base2> give();
    void take(std::unique_ptr<int> new_b);
    u_int_ptr give_back_int(u_int_ptr a) {return a;}
    ub2 give_back_u_base(ub2 a) {return a;}

private:
    std::unique_ptr<int> b;
};

void take_your_ptr(std::unique_ptr<int> int_ptr);

#endif // UNIQUE_H