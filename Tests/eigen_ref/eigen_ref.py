import sys, os, unittest
import numpy

sys.path.append(os.getcwd())

from EigenRefMod import Eigen_Class as ec, transpose, transpose_in_place


class TestEigenRef(unittest.TestCase):
    def test_Eigen_Class(self):
        e = ec()
        g = e.getMatrix()
        v = e.viewMatrix()
        self.assertTrue(g.flags.writeable)
        self.assertFalse(g.flags.owndata)
        self.assertFalse(v.flags.writeable)
        self.assertFalse(g.flags.owndata)
        self.assertEqual(g[4][5], v[4][5])
        vec = numpy.zeros(shape=(5), dtype="float64")
        vec[0] = 1
        e.scale_by_2(vec)
        vec2 = e.getGivenMat(vec)

    def test_eigen_free_functions(self):
        vec = numpy.zeros(shape=(5), dtype="float64")
        vec[1] = 1
        vec[2] = 2
        vec_t = transpose(vec)
        self.assertEqual(vec_t.shape, (1, 5))
        vec_t_i = vec
        transpose_in_place(vec_t_i)
        numpy.testing.assert_array_equal(vec_t_i, vec_t[0])


if __name__ == "__main__":
    unittest.main()
