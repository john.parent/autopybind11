#  Sets environment for AutoPyBind11 development
#  Mostly stolen from:
#  https://ljvmiranda921.github.io/notebook/2018/06/21/precommits-using-black-and-flake8/
#
python -m pip install -r requirements-dev.txt
pre-commit install