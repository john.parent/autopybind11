#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include <array>

class Simple_Int
{
public:
    Simple_Int() = default;
    Simple_Int(int a_) : a(a_) {}
    int get() {return a;}

private:
    int a;
};

template < typename T >
class set
{
public:
  using value_type = T;

  virtual ~set() = default;

  /**
   * Get the number of elements in this set.
   *
   * @returns Number of elements in this set.
   */
  virtual size_t size() const = 0;

  /**
   * Whether or not this set is empty.
   *
   * @return True if this set is empty or false otherwise.
   */
  virtual bool empty() const = 0;

  //@{
  /**
   * Get the element at specified index.
   *
   * Returns a reference to the element at specified location index,
   * with bounds checking.
   *
   * If index is not within the range of the container, an exception of
   * type std::out_of_range is thrown.
   *
   * @param index Position of element to return (from zero).
   *
   * @return Shared pointer to specified element.
   *
   * @throws std::out_of_range if position is now within the range of objects
   * in container.
   */
  virtual T at( size_t index ) = 0;
  virtual T const at( size_t index ) const = 0;

  virtual T ret(T type) = 0;
  virtual T const ret(T type) const = 0;

  T operator[]( size_t index ) { return this->at( index ); }
  const T operator[]( size_t index ) const { return this->at( index ); }
  ///@}
};

using Int = Simple_Int;
class Container : public set<Simple_Int>
{
public:
    Container() {arr = {Simple_Int(0), Simple_Int(1), Simple_Int(2), Simple_Int(3), Simple_Int(4)};};
    virtual Int at(size_t pos) override;
    virtual const Simple_Int at(size_t pos) const override;
    virtual Int ret(Int type) override;
    virtual const Int ret(Simple_Int type) const override;
    size_t size() const override;
    bool empty() const override;


private:
    std::array<Simple_Int,5> arr;
    size_t len = arr.size();
};


#endif // CONTAINER_HPP