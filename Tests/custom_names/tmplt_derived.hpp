/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef TMPLT_DERIVED_HPP
#define TMPLT_DERIVED_HPP


#include <string>
#include "Base.hpp"
namespace nmspc1 {


template<typename T>
class T1 : public Base 
{
public:
    inline T1() {}
    T t1_adder(const T& val, const T& val1)
    {
        return val+val1;
    }

    virtual std::string test_() override 
    {
        return "T1";
    }

};

namespace nmspc2 {
template<typename T, typename S>
class T2 : public Base{
public:

    inline T2() {}
    virtual std::string test_() override 
    {
        return "T2"; //maybe convert T to typestring and return to check proper aliasing on custom name and initialization type
    }


    T t2_adder(T &val1, S &val2)
    {
        T tval2 = T(val2);

        return val1 + tval2;
    }
};


template<typename T, typename S, typename R>
class T3 : public Base{
public:

    inline T3() {}
    virtual std::string test_() override 
    {
        return "T3";
    }

    T t3_sum(T &val1, S &val2, R &val3)
    {
        T val2t = T(val2);
        T val3t = T(val3);
        return val1 + val2t + val3t;
    }

};

inline int overloaded_non_template() { return 0; }

inline int overloaded_non_template(int arg1) { return arg1; }

inline int overloaded_non_template(int arg1, int arg2) { return arg1 + arg2; }


}
}
#endif