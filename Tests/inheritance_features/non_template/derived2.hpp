/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef DERIVED2_HPP
#define DERIVED2_HPP

#include "derived1.hpp"
#include <string>

namespace nmspc1 {
namespace nmspc2 {

class Derived2 : public Derived1
{
public:
  int var2;

  inline Derived2() { var2 = 0; }
  inline virtual std::string whoami() const override
  {
    return std::string("Derived2");
  }

  inline virtual std::string virt2(float f, std::string s) const
  {
    return std::string("Derived2.virt2()");
  }

  // Note that we are relying on derived1's default implementation
  // of virt1(). Making sure that inherited functions that don't appear in
  // the derived class's definition are still overridable/present in the
  // trampoline.
};

inline std::string call_virt_from_derived2(const Derived2& d2)
{
  std::string ret;
  ret += d2.whoami();
  ret += ": ";
  ret += d2.virt1(3.14);
  ret += d2.virt2(3.14, std::string("a_string"));
  return ret;
}

}}
#endif