cmake_minimum_required(VERSION 3.15)
project(Holder-Types CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_library(holder_types shared_holder.cpp unique.cpp)
target_sources(holder_types INTERFACE shared_base.hpp)
target_include_directories(holder_types INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
set_property(TARGET holder_types PROPERTY POSITION_INDEPENDENT_CODE ON)

autopybind11_add_module(Holder
                        YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/holder_wrap.yml
                        CONFIG_YAML ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES holder_types)
