#ifndef SIMPLE_STRING
#define SIMPLE_STRING

#include <string>

class SimpleStringOps
{
public:
    static inline void capitalize(std::string &str) { for(auto &x: str)x=toupper(x);}
    static inline void swap(std::string & str1, std::string & str2) { std::string t1 = str1; str1.swap(str2); str2.swap(t1);}
};

#endif // SIMPLE_STRING