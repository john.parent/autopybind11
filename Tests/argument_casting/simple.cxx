#include <iostream>
#include "simple.h"

simple::simple()
{
}

int simple::hello()
{
  std::cout << "hello\n";
  return 10;
}

void simple::print_four(std::array<double, 4> numArray)
{
  for(int index = 0; index < numArray.size(); index++)
  {
    std::cout << numArray[index];
  }
}

void simple::print_one(double val)
{
  std::cout << val;
}
