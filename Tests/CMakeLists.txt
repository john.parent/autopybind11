function(add_autopybind11_test name)
  add_test(NAME ${name} COMMAND "${CMAKE_CTEST_COMMAND}"
    --build-and-test
    ${CMAKE_CURRENT_SOURCE_DIR}/${name}
    ${CMAKE_CURRENT_BINARY_DIR}/${name}
    --build-two-config
    --build-run-dir ${CMAKE_CURRENT_BINARY_DIR}/${name}
    --build-generator ${CMAKE_GENERATOR}
    --build-project ${name}
    --build-options "-DPYBIND11_SRC_DIR=${pybind11_SOURCE_DIR}"
    "-DAutoPyBind11_DIR=${AutoPyBind11_DIR}"
    "-DCastXML_EXECUTABLE=${CastXML_EXECUTABLE}"
    "-DPYTHON_EXECUTABLE=${PYTHON_EXECUTABLE}"

    --test-command ${PYTHON_EXECUTABLE}
    ${CMAKE_CURRENT_SOURCE_DIR}/${name}/${name}.py
    )
  set_tests_properties(${name} PROPERTIES
    ENVIRONMENT PYTHONPATH=${CMAKE_CURRENT_BINARY_DIR}/${name})

endfunction()

function(add_autopybind11_failing_test name)
  add_autopybind11_test(${name})
  set_tests_properties(${name} PROPERTIES WILL_FAIL TRUE)
endfunction(add_autopybind11_failing_test)

if(Eigen_INCLUDE_DIR)
  add_test(NAME eigen_dependency COMMAND "${CMAKE_CTEST_COMMAND}"
  --build-and-test
  ${CMAKE_CURRENT_SOURCE_DIR}/eigen
  ${CMAKE_CURRENT_BINARY_DIR}/eigen
  --build-two-config
  --build-run-dir ${CMAKE_CURRENT_BINARY_DIR}/eigen
  --build-generator ${CMAKE_GENERATOR}
  --build-project eigen
  --build-options "-DEigen_INCLUDE_DIR=${Eigen_INCLUDE_DIR}"
  --test-command "${CMAKE_CTEST_COMMAND}"
  )

  add_autopybind11_test(eigen_ref)
  set_tests_properties(eigen_ref PROPERTIES ENVIRONMENT Eigen_INCLUDE_DIR=${Eigen_INCLUDE_DIR})
  add_autopybind11_test(eigen_ref_toggle)
  set_tests_properties(eigen_ref_toggle PROPERTIES ENVIRONMENT Eigen_INCLUDE_DIR=${Eigen_INCLUDE_DIR})
  add_autopybind11_test(denoise)
  set_tests_properties(denoise PROPERTIES ENVIRONMENT Eigen_INCLUDE_DIR=${Eigen_INCLUDE_DIR})
endif()

add_autopybind11_test(simple)
add_autopybind11_test(xml_input)
add_autopybind11_test(check_attributes)
add_autopybind11_test(class_attributes)
add_autopybind11_test(commented)
add_autopybind11_test(missing_classes)
add_autopybind11_test(complex_trampoline)
add_autopybind11_test(interface_library)
add_autopybind11_test(cxx11_compliant)
add_autopybind11_test(gen_only)
add_autopybind11_test(multi_module)
add_autopybind11_test(multi_namespace)
add_autopybind11_test(multi_target)
add_autopybind11_test(inheritance_features)
add_autopybind11_test(inheritance_names)
add_autopybind11_test(operators)
add_autopybind11_test(custom_names)
add_autopybind11_test(skip_structure)
add_autopybind11_test(nested_enum)
add_autopybind11_test(argument_casting)
add_autopybind11_test(deleted_constructor)
add_autopybind11_test(module_local)
add_autopybind11_test(skiplist)
add_autopybind11_test(custom_namespaces)
add_autopybind11_test(extra_includes)
add_autopybind11_test(keep_alive)
add_autopybind11_test(test_enumerations)
add_autopybind11_failing_test(extra_includes/expect_failure)
add_autopybind11_test(file_collisions)
add_autopybind11_test(virtuality)
add_autopybind11_test(template_members)
add_autopybind11_failing_test(forward_compiler_opts)
add_autopybind11_test(enum_deps)
add_autopybind11_test(holder_types)
add_autopybind11_test(ref_support)
add_autopybind11_test(tramp_duplication)
add_subdirectory(code_generation_regression)

