"""
Tests Python API and regression on generated code.
"""

import argparse
import os
from os.path import join, isdir, abspath, dirname
import shlex
from shutil import copyfile, copytree, copy, rmtree
from subprocess import run, PIPE
import sys
from tempfile import TemporaryDirectory
from textwrap import dedent
import unittest

import autopybind11.__main__ as mut
from autopybind11._formatting import able_to_format

# To update regression source files, pass --regenerate to the binary.
ARGS = None

INPUT_DIR = join(dirname(abspath(__file__)), "input")
assert isdir(INPUT_DIR)
EXPECTED_DIR = join(dirname(abspath(__file__)), "expected")
assert isdir(EXPECTED_DIR)


def makedirs_clean(d):
    if isdir(d):
        rmtree(d)
    os.makedirs(d)


def whitespace_normalize(s):
    # Naive and ugly, but gets the job done.
    return s.replace(" ", "").replace("\n", "").replace("/", "\\")

class SmokeTest(unittest.TestCase):
    def setUp(self):
        assert ARGS is not None
        # See unittest.maxDiff docs.
        self.maxDiff = None

    def test_regression(self):
        if ARGS.regenerate:
            assert able_to_format(), (
                "You must have clang-format-9 available to update regression "
                "tests."
            )
            tmp_dir = "/tmp/autopybind11"
            self.check_regression(tmp_dir)
        else:
            with TemporaryDirectory() as tmp_dir:
                self.check_regression(str(tmp_dir))

    def check_regression(self, tmp_dir):
        tmp_input_dir = join(tmp_dir, "input")
        output_dir = join(tmp_dir, "output")
        # N.B. We use this to permit easy debugging.
        if isdir(tmp_input_dir):
            rmtree(tmp_input_dir)
        copytree(INPUT_DIR, tmp_input_dir, copy_function=copy)
        makedirs_clean(output_dir)

        castxml_path = ARGS.castxml_path


        # Assumes one source file for now.
        (source_file_relpath,) = os.listdir(tmp_input_dir)
        source_file = join(tmp_input_dir, source_file_relpath)

        cmake_response_file = join(tmp_input_dir, "cmake_response.txt")
        with open(cmake_response_file, "w") as f:
            f.write(dedent(f"""\
            includes: {output_dir}
            defines:
            c_std: --std={ARGS.std}
            """))

        input_yaml_file = join(tmp_input_dir, "config.yml")
        with open(input_yaml_file, "w") as f:
            f.write(dedent(f"""\
            files:
              {source_file}:
                classes:
                  A:
                  B:
            """))

        # WARNING: Because of the current ArgParser-dependent API, this
        # requires mutating `sys.argv`.
        base_argv = [
            "--output", output_dir,
            "--input_yaml", input_yaml_file,
            "--module_name", "smoke_test",
            "--castxml-path", castxml_path,
            "--input_response", cmake_response_file,
            "--no_format",
        ]
        mut.main(argv=base_argv + ["--stage=1"])
        mut.main(argv=base_argv + ["--stage=2"])

        def normalize(actual_text):
            return actual_text.replace(tmp_input_dir, r"${input_dir}")

        def copy_as_template(src_file, dest_file):
            with open(src_file, "r") as f_src, open(dest_file, "w") as f_dest:
                src = f_src.read()
                dest = normalize(src)
                f_dest.write(dest)

        actual_files = os.listdir(output_dir)
        if ARGS.regenerate:
            self.assertNotEqual(actual_files, [])
            # Just reformat output file, and copy them wholesale.
            rmtree(EXPECTED_DIR)
            copytree(output_dir, EXPECTED_DIR, copy_function=copy_as_template)
            print("Files copied! Please re-run this test via CTest.")
        else:
            expected_files = os.listdir(EXPECTED_DIR)
            self.assertSetEqual(set(actual_files), set(expected_files))
            for relpath in sorted(expected_files):
                with open(join(EXPECTED_DIR, relpath)) as f:
                    expected_text = f.read()
                with open(join(output_dir, relpath)) as f:
                    actual_text = normalize(f.read())
                
                if able_to_format():
                    self.assertEqual(expected_text, actual_text)
                else:
                    # Normalize against whitespace. Of course, this assumes that
                    # formatting only affects whitespace
                    self.assertEqual(
                        whitespace_normalize(expected_text),
                        whitespace_normalize(actual_text),
                    )


def parse_args(argv):
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--castxml_path", type=str, required=True)
    parser.add_argument("-std", type=str, required=True)
    parser.add_argument("--regenerate", action="store_true")
    if set(argv) & {"-h", "--help"}:
        parser.print_help()
        # Let `unittest.main()` handle the rest.
        args = None
        argv_out = argv
    else:
        args, argv_out = parser.parse_known_args(argv)
    return args, argv_out


def shlex_join(argv):
    # TODO(eric.cousineau): Replace this with `shlex.join` when we exclusively
    # use Python>=3.8.
    return " ".join(map(shlex.quote, argv))


def print_regenerate_cmd(script, orig_argv):
    cmd = shlex_join(
        [sys.executable, abspath(script)] + orig_argv + ["--regenerate"]
    )
    print("TESTS FAILED")
    print()
    print("To re-generate tests, you must have clang-format installed.")
    print("Then re-run this test, but with --regenerate:")
    print()
    print(f"  {cmd}")
    print()


def main():
    global ARGS
    # Intercept arguments.
    script = __file__
    assert abspath(sys.argv[0]) == abspath(script)
    orig_argv = list(sys.argv[1:])
    ARGS, argv = parse_args(argv=sys.argv[1:])
    # N.B. unittest.main() has a different interpretation of `argv` than
    # `ArgumentParser`.
    try:
        unittest.main(argv=[script] + argv)
    except SystemExit as e:
        if e.code != 0 and ARGS and not ARGS.regenerate:
            print_regenerate_cmd(script, orig_argv)
        raise e


if __name__ == "__main__":
    main()
