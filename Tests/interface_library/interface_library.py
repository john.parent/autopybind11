# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())  # noqa

import interfaceWrap
from interfaceWrap import named as named_module
# Import of test_base_double from additional causes conflict on run
# from additional.additional import nonTemplate


class exampleTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_test_base_double(self):
        tb = interfaceWrap.test_base_double()
        self.assertIn("base_adder", dir(tb))
        self.assertIn("base_var", dir(tb))
        tb.base_var = 4
        self.assertEqual(6, tb.base_adder(2))

    def test_test_double(self):
        # Test initializations
        t = interfaceWrap.test_double()
        self.assertEqual(4, t.adder(2, 2))
        self.assertAlmostEqual(4.5, t.adder(2, 2.5))
        self.assertEqual(9, t.summer(2, 2, 5))
        # test summer with defaults: <no default>, 0 , 14
        self.assertEqual(16, t.summer(2))

        with self.assertRaises(TypeError):
            # The final two parameters here are int, not float
            t.summer(1.0, 0, 1.0)

        # Test Inheritance
        t.base_var = 2
        self.assertEqual(4, t.base_adder(2))

        # Set/Get private variables
        t.priv_var1 = 2
        self.assertEqual(2, t.priv_var1)
        # Cannot set priv_var2, only read.
        with self.assertRaises(AttributeError):
            t.priv_var2 = 1
        print(t.priv_var2)

        # Test other initializations
        t1 = interfaceWrap.test_double(14)
        self.assertEqual(14, t1.val1_var)
        self.assertEqual(0, t1.val2_var)

        t2 = interfaceWrap.test_double(15, 20)
        self.assertEqual(15, t2.val1_var)
        self.assertEqual(0, t2.val2_var)

    def test_named(self):
        self.assertEqual(4, interfaceWrap.named.outside_adder_2(2, 2))

    def test_interfaceWrap(self):
        self.assertIn("template_f", dir(interfaceWrap))
        self.assertIn("template_f_multi_arg", dir(interfaceWrap))
        self.assertIn("multi_template", dir(interfaceWrap))
        self.assertIn("overloaded_non_template", dir(interfaceWrap))
        self.assertIn("no_namespace_adder", dir(interfaceWrap))

        # Test overloads of overloaded_non_template
        self.assertAlmostEqual(6, interfaceWrap.overloaded_non_template(3, 3))
        self.assertAlmostEqual(2, interfaceWrap.overloaded_non_template(2))
        self.assertAlmostEqual(0, interfaceWrap.overloaded_non_template())

        self.assertEqual(4, interfaceWrap.no_namespace_adder(2, 2))
        self.assertEqual(3, interfaceWrap.no_namespace_adder(2))
        self.assertEqual(1, interfaceWrap.no_namespace_adder())

    def test_interfaceWrap_enums(self):
        self.assertIn("namedType", dir(named_module))
        self.assertIn("unscopedType", dir(named_module))
        self.assertEqual(named_module.Four, named_module.unscopedType(0))
        self.assertEqual(named_module.Five, named_module.unscopedType(1))
        self.assertEqual(named_module.Six, named_module.unscopedType(2))
        self.assertEqual(named_module.One, named_module.namedType(0))
        self.assertEqual(named_module.Two, named_module.namedType(1))
        self.assertEqual(named_module.Three, named_module.namedType(2))

    def test_typedef_classes(self):
        self.assertIn("hour_duration", dir(named_module))
        self.assertIn("minute_duration", dir(named_module))
        hour = named_module.hour_duration()
        minute = named_module.minute_duration()
        self.assertEqual(0, hour.count())
        hour + 1
        self.assertEqual(1, hour.count())

        self.assertEqual(0, minute.count())
        minute + 1
        self.assertEqual(1, minute.count())


if __name__ == '__main__':
    testRunner = unittest.main(__name__, argv=['main'], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
