#include <iostream>
#include "simple.h"

uSimple::uSimple()
{
}

/// cxx comment
/// with multiple lines
int uSimple::hello()
{
  std::cout << "hello\n";
  return 10;
}

void uSimple::invisible()
{
  std::cout << "I'm not here\n";
}

void uSimple::doubleLine()
{
  std::cout << "or here\n";
}

std::shared_ptr<std::vector<double>> uSimple::get_vec()
{
    return this->v;
}