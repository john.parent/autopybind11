#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/stl.h>
#include "noisey.hpp"
#include <pybind11/eigen.h>






namespace py = pybind11;


void apb11_denoise_mod_NoisyClass_py_register(py::module &m)
{
  py::class_<NoisyClass> PyNoisyClass(m, "NoisyClass");
        
    
    PyNoisyClass.def(py::init<NoisyClass const &>(),py::arg("arg0"))
    .def(py::init<>())
    .def("noisyEigen", static_cast<Eigen::Ref< RowMatrixXd > ( NoisyClass::* )( RowMatrixXd )>(&NoisyClass::noisyEigen), py::arg("m"))
    .def("noisyStd", static_cast<void ( NoisyClass::* )( std::list<Coupler<Mid>, std::allocator<Coupler<Mid>>>,std::vector<int, std::allocator<int>> )>(&NoisyClass::noisyStd), py::arg("m") = (std::list<Coupler<Mid>, std::allocator<Coupler<Mid>>>)std::list<MidCouple>(), py::arg("vec") = (std::vector<int, std::allocator<int>>)std::vector<int>())
    
    
    ;
}
